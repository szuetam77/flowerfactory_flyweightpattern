package factory;

public class FlowerPrice {
    private int price;
    private Flower flower;

    public FlowerPrice(int price, Flower flower) {
        this.price = price;
        this.flower = flower;
    }
}