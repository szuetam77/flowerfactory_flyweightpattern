package factory;

public class FlowerDemo {

    public static void main(String[] args) {

        PriceList priceListBasic = new PriceList();
        priceListBasic.setPrice(FlowerFactory.get("Roza","Czerwona"), 6);
        priceListBasic.setPrice(FlowerFactory.get("Roza","Zolta"), 5);

        PriceList priceListDiscount = new PriceList();
        priceListDiscount.setPrice(FlowerFactory.get("Roza","Czerwona"), 5);
        priceListDiscount.setPrice(FlowerFactory.get("Roza","Zolta"), 4.5);

        Customer customer = new Customer();

        customer.addToChart(FlowerFactory.get("Roza","Czerwona"));
        customer.addToChart(FlowerFactory.get("Roza","Czerwona"));
        customer.addToChart(FlowerFactory.get("Roza","Czerwona"));
        customer.addToChart(FlowerFactory.get("Roza","Zolta"));

        double amountToPay = customer.calculatePrice(priceListDiscount);
        System.out.println("Has to pay: "+amountToPay);

    }
}