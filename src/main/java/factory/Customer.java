package factory;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Value;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@Value
public class Customer {

    private final List<Flower> chart = new ArrayList<>();

    void addToChart(Flower flower) {
        chart.add(flower);
    }

    public double calculatePrice(PriceList priceListBasic) {
        return chart.stream().map(priceListBasic::getPrice).mapToDouble(Double::valueOf).sum();
    }
}
