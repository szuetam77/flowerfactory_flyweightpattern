package factory;

import java.util.HashMap;


public class FlowerFactory {

    private static HashMap<String, Flower> FLOWER_HASH_MAP = new HashMap<>();

    public static Flower get(String name, String colour) {
        if (!FLOWER_HASH_MAP.containsKey(name + colour))
            FLOWER_HASH_MAP.put(name + colour, new Flower(name, colour));
        return FLOWER_HASH_MAP.get(name + colour);
    }

    public int totalFlowersMade() {
        return FLOWER_HASH_MAP.size();
    }
}