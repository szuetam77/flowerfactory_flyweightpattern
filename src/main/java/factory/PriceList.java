package factory;

import java.util.HashMap;
import java.util.Map;

public class PriceList {
    private Map<Flower, Double> prices;

    public PriceList(){
        prices = new HashMap<>();
    }

    public void setPrice(Flower f, double price){
        prices.put(f, price);
    }

    public double getPrice(Flower f){
        return prices.getOrDefault(f, 0.0);
    }
}
